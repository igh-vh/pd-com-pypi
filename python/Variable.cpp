/*****************************************************************************
 *
 * $Id$
 *
 * Copyright (C) 2015-2020  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <structmember.h>       // struct PyMemberDef
#include <sstream>              // std::ostringstream

#include "../pdcom4/Variable.h"
#include "type_traits.h"

#define OBJ(self) ((PyObject*)self)
#define SELF(o)   ((VariableObject*)o)
#define PDCOM(o) (((VariableObject*)o)->pdcom)

void Process_forgetVariable(PyObject *, const PdCom::Variable *);
PyObject *SubscriptionType_Object();

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
typedef struct {
    PyObject_HEAD //;
    PyObject* process;
    const PdCom::Variable* pdcom;
    int type;
    int task;
    const char *ctype;
    const char *alias;
    PyObject *path;
    PyObject *name;
    PyObject *dim;
    PyObject *(*setValue)(
            const PdCom::Variable* var, PyObject* value, size_t index);
    double sampleTime;
    Py_ssize_t nelem;
    char writeable;
    char isScalar;
    char isVector;
} VariableObject;

////////////////////////////////////////////////////////////////////////////
    static void
VariableType_dealloc(VariableObject *self)
{
//    printf("%s(%p)\n", __func__, self);
    Process_forgetVariable(self->process, PDCOM(self));
    Py_DECREF(self->process);

    Py_XDECREF(self->path);
    Py_XDECREF(self->name);
    Py_XDECREF(self->dim);

    Py_TYPE(self)->tp_free(self);
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
VariableType_str(VariableObject *self)
{
//    printf("%s(%p)\n", __func__, self);
    std::ostringstream os;

    os << PDCOM(self)->path();

    if (!self->isScalar) {
        for (std::vector<size_t>::const_iterator it = PDCOM(self)->dim.begin();
                it != PDCOM(self)->dim.end(); ++it)
            os << '[' << *it << ']';
    }

    return PyUnicode_FromString(os.str().c_str());
}

////////////////////////////////////////////////////////////////////////////
// Functions to convert from PyNumbers to c-types
// They rely on definitions in type_traits.h
////////////////////////////////////////////////////////////////////////////
template <typename T>
    static PyObject *
Set_Value(T (*from_pyobject)(PyObject*),
        const PdCom::Variable *var, PyObject *val, size_t idx)
{
    size_t nelem = var->nelem - idx;
    T value[nelem];
    if (PyNumber_Check(val)) {
        // Convert from possible scalar
        value[0] = from_pyobject(val);
        nelem = 1;
    } else {
        // Convert from iterable
        PyObject *iterator = PyObject_GetIter(val);
        PyObject *item;
        if (!iterator)
            return NULL;

        T* valptr = value;
        while ((item = PyIter_Next(iterator))) {
            if (valptr >= value + nelem) {
                PyErr_SetString(PyExc_IndexError, "index out of range");
                break;
            }

            *valptr++ = from_pyobject(item);
            Py_DECREF(item);
        }
        nelem = valptr - value;

        Py_DECREF(iterator);
    }

    if (PyErr_Occurred())
        return NULL;

    PyObject *rv = var->setValue(value, idx, nelem)
        ? Py_True : Py_False;
    Py_INCREF(rv);
    return rv;
}

template <typename T>
    static PyObject *
SetValue(const PdCom::Variable *v, PyObject *val, size_t idx)
{
    return Set_Value(traits<T>::PyObject_As, v, val, idx);
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
    static PyObject *
variable_setValue(VariableObject *self, PyObject *args)
{
    PyObject *val;
    unsigned long idx = 0;
    if (!PyArg_ParseTuple(args, "O|k", &val, &idx))
        return NULL;

    if (idx >= PDCOM(self)->nelem) {
        PyErr_SetString(PyExc_IndexError, "index out of range");
        return NULL;
    }

    if (!self->setValue) {
        switch (PDCOM(self)->type) {
            case PdCom::Variable::boolean_T:
                self->setValue = SetValue<    bool>;
                break;

            case PdCom::Variable::uint8_T:
                self->setValue = SetValue< uint8_t>;
                break;

            case PdCom::Variable:: int8_T:
                self->setValue = SetValue<  int8_t>;
                break;

            case PdCom::Variable::uint16_T:
                self->setValue = SetValue<uint16_t>;
                break;

            case PdCom::Variable:: int16_T:
                self->setValue = SetValue< int16_t>;
                break;

            case PdCom::Variable::uint32_T:
                self->setValue = SetValue<uint32_t>;
                break;

            case PdCom::Variable:: int32_T:
                self->setValue = SetValue< int32_t>;
                break;

            case PdCom::Variable::uint64_T:
                self->setValue = SetValue<uint64_t>;
                break;

            case PdCom::Variable:: int64_T:
                self->setValue = SetValue< int64_t>;
                break;

            case PdCom::Variable::double_T:
            case PdCom::Variable::single_T:
                self->setValue = SetValue<  double>;
                break;
        }
    }

    return self->setValue(PDCOM(self), val, idx);
}

////////////////////////////////////////////////////////////////////////////
static PyMethodDef tp_methods[] = {
    {"setValue", (PyCFunction)variable_setValue, METH_VARARGS,
        PyDoc_STR("setValue(value, idx:int) -> bool\n\n"
                "Set the value(s) of a parameter, starting at optional\n"
                "offset idx.\n"
                "value may be a scalar or an iterable and must evaluate to\n"
                "a number.\n"
                "Returns true on error.")},
        {0,0}
};

////////////////////////////////////////////////////////////////////////////
static PyMemberDef tp_members[] = {
    {(char*)"process", T_OBJECT, offsetof(VariableObject, process),
        READONLY, (char*)"process object"},
    {(char*)"type", T_INT, offsetof(VariableObject, type),
        READONLY, (char*)"data type. See pdcom.Variable.*_T"},
    {(char*)"ctype", T_STRING, offsetof(VariableObject, ctype),
        READONLY, (char*)"C-type name"},
    {(char*)"sampleTime", T_DOUBLE, offsetof(VariableObject, sampleTime),
        READONLY, (char*)"Variable sample time"},
    {(char*)"writeable", T_BOOL, offsetof(VariableObject, writeable),
        READONLY, (char*)"Variable is writeable (parameters)"},
    {(char*)"alias", T_STRING, offsetof(VariableObject, alias),
        READONLY, (char*)"Variable alias"},
    {(char*)"path", T_OBJECT, offsetof(VariableObject, path),
        READONLY, (char*)"Variable path"},
    {(char*)"name", T_OBJECT, offsetof(VariableObject, name),
        READONLY, (char*)"Variable name (last part of path)"},
    {(char*)"nelem", T_PYSSIZET, offsetof(VariableObject, nelem),
        READONLY, (char*)"Total number of elements"},
    {(char*)"dim", T_OBJECT, offsetof(VariableObject, dim),
        READONLY, (char*)"Dimension vector"},
    {(char*)"task", T_INT, offsetof(VariableObject, task),
        READONLY, (char*)"Task variable is calculated in"},
    {(char*)"isScalar", T_BOOL, offsetof(VariableObject, isScalar),
        READONLY, (char*)"Variable is scalar"},
    {(char*)"isVector", T_BOOL, offsetof(VariableObject, isVector),
        READONLY, (char*)"Variable is vector"},
    {NULL}
};

static PyType_Slot tp_slots[] = {
    {Py_tp_doc,     (void*)"The Variable type"},
    {Py_tp_methods, tp_methods},
    {Py_tp_members, tp_members},
    {Py_tp_dealloc, (void*)VariableType_dealloc},
    {Py_tp_str,     (void*)VariableType_str},
    {0, 0}
};

static PyType_Spec Variable_Type_spec = {
    "pdcom.Variable",
    sizeof(VariableObject),     /* basicsize */
    0,                          /* itemsize */
    Py_TPFLAGS_DEFAULT,
    tp_slots
};

////////////////////////////////////////////////////////////////////////////
    extern "C" PyObject *
VariableType_Object()
{
    static PyObject* obj;
    if (!obj) {
        obj = PyType_FromSpec(&Variable_Type_spec);

        PyObject_SetAttrString(obj,
                "boolean_T", PyLong_FromLong(PdCom::Variable::boolean_T));
        PyObject_SetAttrString(obj,
                "uint8_T",   PyLong_FromLong(PdCom::Variable::  uint8_T));
        PyObject_SetAttrString(obj,
                "int8_T",    PyLong_FromLong(PdCom::Variable::   int8_T));
        PyObject_SetAttrString(obj,
                "uint16_T",  PyLong_FromLong(PdCom::Variable:: uint16_T));
        PyObject_SetAttrString(obj,
                "int16_T",   PyLong_FromLong(PdCom::Variable::  int16_T));
        PyObject_SetAttrString(obj,
                "uint32_T",  PyLong_FromLong(PdCom::Variable:: uint32_T));
        PyObject_SetAttrString(obj,
                "int32_T",   PyLong_FromLong(PdCom::Variable::  int32_T));
        PyObject_SetAttrString(obj,
                "uint64_T",  PyLong_FromLong(PdCom::Variable:: uint64_T));
        PyObject_SetAttrString(obj,
                "int64_T",   PyLong_FromLong(PdCom::Variable::  int64_T));
        PyObject_SetAttrString(obj,
                "double_T",  PyLong_FromLong(PdCom::Variable:: double_T));
        PyObject_SetAttrString(obj,
                "single_T",  PyLong_FromLong(PdCom::Variable:: single_T));

        PyObject_SetAttrString(obj,
                "Subscription",  SubscriptionType_Object());
    }
    return obj;
}

////////////////////////////////////////////////////////////////////////////
    PyObject *
Variable_New(PyObject *process, const PdCom::Variable *var)
{
    VariableObject *self = PyObject_New(
            VariableObject, (PyTypeObject*)VariableType_Object());

    Py_INCREF(process);

    self->process = process;
    self->pdcom = var;
    self->type = var->type;
    self->task = var->task;
    self->ctype = var->ctype;
    self->alias = var->alias.c_str();
    self->path = PyUnicode_FromString(var->path().c_str());
    self->name = PyUnicode_FromString(var->name().c_str());
    self->sampleTime = var->sampleTime;
    self->nelem = var->nelem;
    self->writeable = var->writeable;
    self->isScalar = var->isScalar();
    self->isVector = var->isVector();
    self->setValue = 0;

    size_t count = var->dim.size();
    self->dim = PyTuple_New(count);
    for (size_t i = 0; self->dim and i < count; ++i)
        PyTuple_SET_ITEM(self->dim, i, PyLong_FromSize_t(var->dim[i]));

    return OBJ(self);
}

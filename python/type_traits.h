/*****************************************************************************
 *
 * $Id$
 *
 * Copyright (C) 2015-2020  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <type_traits>

////////////////////////////////////////////////////////////////////////////
// Here comes lots of templating stuff required to set a variable
////////////////////////////////////////////////////////////////////////////


/// Helper to convert a PyNumber to a C type
template <typename T>
    static T
PyNumber_As(
            PyObject *(*to_number)(PyObject*),
            T (*to_value)(PyObject*),
            PyObject *o)
{
    PyObject *n = to_number(o);
    if (!n)
        return T(0);
    T value = to_value(n);
    Py_DECREF(n);
    return value;
}

/// Traits for PyLong types
template <bool is_signed, bool is_longlong>
struct long_traits;

template <>
struct long_traits<false,false> {
    static unsigned long PyObject_As(PyObject* o) { 
        return PyNumber_As(PyNumber_Long, PyLong_AsUnsignedLong, o); }
    static PyObject *PyObject_From(unsigned      long val) {
        return PyLong_FromUnsignedLong(val); }
};
template <>
struct long_traits<false, true> {
    static unsigned long long PyObject_As(PyObject* o) { 
        return PyNumber_As(PyNumber_Long, PyLong_AsUnsignedLongLong, o); }
    static PyObject *PyObject_From(unsigned long long val) {
        return PyLong_FromUnsignedLongLong(val); }
};
template <>
struct long_traits< true,false> {
    static long PyObject_As(PyObject* o) {
        return PyNumber_As(PyNumber_Long, PyLong_AsLong, o); }
    static PyObject *PyObject_From(              long val) {
        return PyLong_FromLong(val); }
};
template <>
struct long_traits< true, true> {
    static unsigned long PyObject_As(PyObject* o) { 
        return PyNumber_As(PyNumber_Long, PyLong_AsLongLong, o); }
    static PyObject *PyObject_From(         long long val) {
        return PyLong_FromLongLong(val); }
};

/// General traits, by default long_traits
template <typename T>
struct traits :
    long_traits<std::is_signed<T>::value, (sizeof(T) > sizeof(long))> { };

template <>     // Specialization for boolean type
struct traits<  bool> {
    static bool PyObject_As(PyObject* o) {
        return PyObject_IsTrue(o); }
    static PyObject *PyObject_From(bool val) {
        PyObject *o = val ? Py_True : Py_False;
        Py_INCREF(o);
        return o;
    }
};
template <>     // Specialization for double type
struct traits<double> {
    static double PyObject_As(PyObject* o) {
        return PyNumber_As(PyNumber_Float, PyFloat_AsDouble, o); }
    static PyObject *PyObject_From(double val) {
        return PyFloat_FromDouble(val); }
};

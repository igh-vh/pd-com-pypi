/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef MSRPROTO_EXPAT_WRAPPER_IMPL_H
#define MSRPROTO_EXPAT_WRAPPER_IMPL_H

#include "expat_wrapper.h"

#include <expat.h>
#include <expat_external.h>
#include <stdexcept>

namespace msr
{

inline void XmlParserDeleter::operator()(XML_ParserStruct* s) noexcept
{
    if (s)
        XML_ParserFree(s);

}

template<typename Handler>
template<typename ... Args, void (Handler::*callback)(Args...)>
struct ExpatWrapper<Handler>::CallbackWrapper<void (Handler::*)(Args...), callback>
{
    static void XMLCALL call(void * userdata, Args ... args)
    {
        auto& This = *reinterpret_cast<ExpatWrapper<Handler>*>(userdata);
        if (This.pending_exception_)
            return;
        try {
            (static_cast<Handler&>(This).*callback)(std::forward<Args>(args)...);
        } catch(...) {
            This.pending_exception_ = std::current_exception();
            XML_StopParser(This.parser_.get(), false);
        }
    }
};

template<typename Handler>
ExpatWrapper<Handler>::ExpatWrapper(const char *encoding)
    : parser_(XML_ParserCreate(encoding))
{
    if (!parser_)
        throw std::runtime_error("Could not create XML parser");

    XML_SetUserData(parser_.get(), this);
    XML_SetStartElementHandler(parser_.get(),
        &CallbackWrapper<decltype(&Handler::startElement), &Handler::startElement>::call);
    XML_SetEndElementHandler(parser_.get(),
        &CallbackWrapper<decltype(&Handler::endElement), &Handler::endElement>::call);
    XML_SetCharacterDataHandler(parser_.get(),
        &CallbackWrapper<decltype(&Handler::characterData), &Handler::characterData>::call);
}


template<typename Handler>
inline bool ExpatWrapper<Handler>::parse(const char *s, std::size_t n, bool final)
{
    if (XML_STATUS_OK == XML_Parse(parser_.get(), s, n, final))
        return true;

    const XML_Error err_code = XML_GetErrorCode(parser_.get());
    if (err_code == XML_ERROR_ABORTED && pending_exception_)
        std::rethrow_exception(std::exchange(pending_exception_, nullptr));

    static_cast<Handler*>(this)->xmlError(XML_ErrorString(err_code));
    return false;
}
} // namespace msr

#endif // MSRPROTO_EXPAT_WRAPPER_H

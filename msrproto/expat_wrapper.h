/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef MSRPROTO_EXPAT_WRAPPER_H
#define MSRPROTO_EXPAT_WRAPPER_H

#include <memory>
#include <functional>
#include <exception>

extern "C"
{
struct XML_ParserStruct;
}

namespace msr
{
struct XmlParserDeleter
{
    void operator()(XML_ParserStruct*) noexcept;
};

template<typename Handler>
class ExpatWrapper
{
public:
    ExpatWrapper(const char *encoding = "UTF-8");

protected:

    bool parse(const char *s, std::size_t n, bool final = false);

private:

    template <typename T, T> struct CallbackWrapper;
    std::unique_ptr<XML_ParserStruct, XmlParserDeleter> parser_;
    std::exception_ptr pending_exception_ = nullptr;
};



} // namespace msr


#endif // MSRPROTO_EXPAT_WRAPPER_H
